import React from 'react';
import { withAuthorization } from '../Session';
 
const HomePage = () => (
  <div>
    <h1>Home Page</h1>
    <p>The Home Page is accessible by every signed in user.</p>
  </div>
);

const condition = authUser => !!authUser // check si authuser n'est pas null
 
export default withAuthorization(condition)(HomePage);