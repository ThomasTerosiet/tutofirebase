import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';

//mdp oublie et inscription pour faire l appel dans le const SignInPage
import { SignUpLink } from '../SignUp';
import { PasswordForgetLink} from '../PasswordForget';

import { withFirebase } from '../Firebase';
import { Form, Input, Button} from 'antd';
import * as ROUTES from '../../constants/routes';

 
const SignInPage = () => (
  <div>
    <h1>SignIn</h1>
    <SignInForm />
    <PasswordForgetLink />
    <SignUpLink />
  </div>
);
  
const signInFormComponent = props => {

  const onFinish = values => {
    const {email, password} = values;
    console.log(email, password);
    props.firebase
    .doSignInWithEmailAndPassword(email, password)
    .then(() => {
      props.history.push(ROUTES.HOME);
    })
    .catch(error => {
      this.setState({ error });
    });

  }

  const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };
  const tailLayout = {
    wrapperCol: {
      offset: 8,
      span: 16,
    },
  };

  return (
  <Form
  {...layout}
  name="basic"
  initialValues={{
    remember: true,
  }}
  onFinish = {onFinish}
>
  <Form.Item 
    label="Email"
    name="email"
    rules={[
      {
          required: true,
          message: 'Please input your username!',
      },
    ]}
  >
      <Input/>
    </Form.Item>
    <Form.Item
        label="Password"
        name="password"
        rules={[
          {
            required: true,
            message: 'Please input your password!',
          },
        ]}
    >
      <Input.Password/>
    </Form.Item>
  
    <Form.Item {...tailLayout}>
      <Button type="primary" htmlType="submit">
          Submit
      </Button>
    </Form.Item>
   </Form>
  );
}
// class SignInFormBase extends Component {
//   constructor(props) {
//     super(props);
 
//     this.state = { ...INITIAL_STATE };
//   }
 
//   onSubmit = event => {
//     const { email, password } = this.state;
 
//     this.props.firebase
//       .doSignInWithEmailAndPassword(email, password)
//       .then(() => {
//         this.setState({ ...INITIAL_STATE });
//         this.props.history.push(ROUTES.HOME);
//       })
//       .catch(error => {
//         this.setState({ error });
//       });
 
//     event.preventDefault();
//   };
 
//   onChange = event => {
//     this.setState({ [event.target.name]: event.target.value });
//   };
 
//   render() {
//     const { email, password, error } = this.state;
 
//     const isInvalid = password === '' || email === '';

//     const layout = {
//       labelCol: {
//         span: 8,
//       },
//       wrapperCol: {
//         span: 16,
//       },
//     };
//     const tailLayout = {
//       wrapperCol: {
//         offset: 8,
//         span: 16,
//       },
//     };
    
//     // const Demo = () => {
//     //   const onFinish = values => {
//     //     console.log('Succes:', values);
//     //   };
//     //   const onFinishFailed = errorInfo => {
//     //     console.log('Failed:', errorInfo);
//     //   }
//     // };
//     const [form] = Form.useForm();
//     console.log(this.props)
//     return (
//       <Form form={form}
//         {...layout}
//         name="basic"
//         initialValues={{
//           remember: true,
//         }}
//         onFinish = {
//           () => {
//             console.log(form)
//           }
//         }
//       >
//         <Form.Item 
//           label="Email"
//           name="Email"
//           rules={[
//             {
//                 required: true,
//                 message: 'Please input your username!',
//             },
//           ]}
//         >
//             <Input value={email} onChange={this.onChange}/>
//           </Form.Item>
//           <Form.Item
//               label="Password"
//               name="password"
//               rules={[
//                 {
//                   required: true,
//                   message: 'Please input your password!',
//                 },
//               ]}
//           >
//             <Input.Password value={password} onChange={this.onChange} />
//           </Form.Item>
        
//           <Form.Item {...tailLayout}>
//             <Button type="primary" htmlType="submit">
//                 Submit
//             </Button>
//           </Form.Item>
//          </Form>
//         );
//       };
//     }
 
const SignInForm = compose(
  withRouter,
  withFirebase,
)(signInFormComponent);
 
export default SignInPage;
 
export { SignInForm };