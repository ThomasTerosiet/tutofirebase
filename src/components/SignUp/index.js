import React, { Component, useState } from 'react';
import { Link, withRouter } from 'react-router-dom';
import {withFirebase} from '../Firebase'
import *as ROUTES from '../../constants/routes';
import {compose } from 'recompose';

import { Form, Input, Button} from 'antd';

const SignUpPage = () => (
  <div>
    <h1>SignUp</h1>
    <SignUpForm />
  </div>
);

const SignUpFormComponent = props => {
  
  const [passwordOne, setpasswordOne] = useState(null);
  const [passwordTwo, setpasswordTwo] = useState(null);
  const[error, seterror] = useState(null);

  const onFinish = values => {
    const {email, passwordOne, username,passwordTwo} = values;
    console.log(email, passwordOne);
    props.firebase
      .doCreateUserWithEmailAndPassword(email, passwordOne)
      .then(authUser => {
        props.firebase
        .user(authUser.user.uid)
        .set({
          username,
          email,
    })
    .catch(err => {
      seterror(err); 
    });
  })
}
    

  const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };
  const tailLayout = {
    wrapperCol: {
      offset: 8,
      span: 16,
    },
  };

  return (
    <Form
      {...layout}
      name="basic"
      initialValues={{
      remember: true,
      }}
      onFinish = {onFinish}
    >   
  <Form.Item 
      label="Email"
      name="email"
      rules={[
      {
          required: true,
          message: 'Please input your email!',
      },
    ]}
  >
      <Input/>
  </Form.Item>

  <Form.Item 
      label="Username"
      name="username"
      rules={[
      {
          required: true,
          message: 'Please input your username!',
      },
    ]}
  >
      <Input/>
  </Form.Item>

  <Form.Item
      label="PasswordOne"
      name="passwordOne"
      rules={[
        {
          required: true,
          message: 'Please input your password!',
        },
      ]}
  >
      <Input.Password/>
  </Form.Item>

  <Form.Item
      label="passwordTwo"
      name="passwordTwo"
      rules={[
        {
        required: passwordOne === passwordTwo,
        message: 'Your password is not the same!',
        },
      ]}
  >
      <Input.Password/>
  </Form.Item>
  
    <Form.Item {...tailLayout}>
      <Button type="primary" htmlType="submit">
          Submit
      </Button>
    </Form.Item>
  
   </Form>
  );
}
  


// const INITIAL_STATE = {
//   username : '',
//   email: '',
//   passwordOne: '',
//   passwordTwo: '',
//   error: null,
// };

// class SignUpFormBase extends Component {
//   constructor(props) {
//     super (props);
//     this.state = {...INITIAL_STATE}; //pourquoi state ici
//   }
//   onSubmit = event => {
//     const { username, email, passwordOne } = this.state;
 
//     this.props.firebase
//       .doCreateUserWithEmailAndPassword(email, passwordOne)
//       .then(authUser => {
//         return this.props.firebase
//         .user(authUser.user.uid)
//         .set({
//           username,
//           email,
//         });
//       })
//       .catch(error => {
//         this.setState({ error });
//       });
 
//     event.preventDefault();
//   };

//   onChange = event => {
//     this.setState({[event.target.name]: event.target.value});
//   };
  
//   render() {
//     const {
//       username,
//       email,
//       passwordOne,
//       passwordTwo,
//       error,
//     } = this.state; // le this.state ici permet l'extraction des valeurs (ES6)

//     const isInvalid = 
//       passwordOne !== passwordTwo || passwordOne === '' ||email === '' || username === '';
//     return (
//       <form onSubmit={this.onSubmit}>
//         <input 
//         name = "username"
//         value={username}
//         onChange= {this.onChange}
//         type='text'
//         placeholder='Nom'
//         />
//         <input
//           name="email"
//           value={email}
//           onChange={this.onChange}
//           type="text"
//           placeholder="Email Address"
//         />
//         <input
//           name="passwordOne"
//           value={passwordOne}
//           onChange={this.onChange}
//           type="password"
//           placeholder="Password"
//         />
//         <input
//           name="passwordTwo"
//           value={passwordTwo}
//           onChange={this.onChange}
//           type="password"
//           placeholder="Confirm Password"
//         />
//         <button disabled={isInvalid} type="submit">Sign Up </button>
 
//         {error && <p>{error.message}</p>}
//       </form>


const SignUpLink = () => (
  <p>
    Don't have an account yet? <Link to={ROUTES.SIGN_UP}>Sign Up</Link>
  </p>
);
const SignUpForm = compose(
  withRouter,
  withFirebase,
)(SignUpFormComponent);

export default SignUpPage;
export { SignUpForm, SignUpLink};
 
