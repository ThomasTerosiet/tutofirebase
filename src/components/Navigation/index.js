import React from 'react';
import { Link } from 'react-router-dom';
import SignOutButton from '../SignOut';
import *as ROUTES from '../../constants/routes';
import { AuthUserContext } from '../Session';

const Navigation = ({authUser}) => (
<div>
  <AuthUserContext.Consumer>
    {authUser => authUser ? <NavigationAuth/> : <NavigationNonAuth/>}
  </AuthUserContext.Consumer>
</div> // affichage different en fonction de connecter ou pas
  );

const NavigationAuth = () => (
    <ul>
      <li>
        <Link to={ROUTES.HOME}>Home</Link>
      </li>
      <li>
        <Link to={ROUTES.ACCOUNT}>Account</Link>
      </li>
      <li>
        <Link to={ROUTES.ADMIN}>Admin</Link>
      </li>
      <li>
        <SignOutButton />
      </li>
    </ul>
);

const NavigationNonAuth = () => (
  <ul>
    <li>
      <Link to={ROUTES.SIGN_IN}>Sign in</Link>
    </li>
  <li>
  <Link to={ROUTES.LANDING}>Landing</Link>
  </li>
</ul>
);
 
export default Navigation;