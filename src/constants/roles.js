import React from 'react';
import * as ROLES from '../../constants/roles';
import { withAuthorization } from '../components/Session';

const AdminPage = () => (
    <div>
        <h1>Admin</h1>
        <p>
            Restricted area ! Only users withs the admin role are authorized.
        </p>
    </div>
);

const condition = authUser => authUser && !!authUser.roles[ROLES.ADMIN];

export default withAuthorization(condition)(AdminPage);